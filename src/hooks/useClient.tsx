import { useEffect, useState } from 'react'
import { API } from '../api/API';
import { IClientResponse } from '../interface/IClient';

export const useClient = () => {
    
    const [isLoading, setIsLoading] = useState(false)
    const [listClients, setListClients] = useState<IClientResponse[]>([] as IClientResponse[])

    const loadClient = async() => {
        setIsLoading(true)
        const resp: any  = await API.get<IClientResponse[]>(`clients`);
        setListClients(resp)
        setIsLoading(false)
    }

    const createClient = async(cliente: IClientResponse) => {
        setIsLoading(true)
        const resp: any  = await API.post<IClientResponse>(`clients`, {
            ...cliente
        });
        setListClients([...listClients, resp])
        setIsLoading(false)
    }

    const updateClient = async(cliente: IClientResponse) => {
        setIsLoading(true)
        const resp: any  = await API.put<IClientResponse[]>(`clients/${cliente.id}`,{
            ...cliente
        });
        setListClients([...listClients, resp])
        setIsLoading(false)
    }

    const removeClient = async(cliente: IClientResponse) => {
        setIsLoading(true)
        const resp: any  = await API.delete<IClientResponse[]>(`clients/${cliente.id}`);
        setListClients(listClients.filter((value, i) => value.id !== cliente.id))
        setIsLoading(false)
    }

    useEffect(() => {
        loadClient()
    }, [])

    return {
        isLoading,
        listClients,
        loadClient,
        createClient,
        updateClient,
        removeClient
    }
}
