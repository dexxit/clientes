
export interface IClientResponse {
    id:                    number | null;
    nombre:                string;
    apellido:              string;
    numero_identificacion: string;
}
