import { useClient } from './hooks/useClient';
import React, { useEffect, useState } from 'react';
import { FormularioCliente } from './components/FormularioCliente';
import { ListaCliente } from './components/ListaCliente';
import { MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBModalHeader } from 'mdbreact';
import { IClientResponse } from './interface/IClient';

function App() {
  const clients = useClient();
  const [open, setOpen] = useState(false);
  const [cliente, setCliente] = useState<IClientResponse>({
      id: null,
      nombre: '',
      apellido: '',
      numero_identificacion: '',
  })

  const handlerOpen = (clienteDefault: IClientResponse | null = null) => {
    setOpen(!open);
    if (clienteDefault != null)
      setCliente(clienteDefault)
  }

  const handlerAction = (cliente: IClientResponse, accion: string) => {
    switch (accion) {
      case 'eliminar':
        clients.removeClient(cliente)
      break;
      case 'crear':
        clients.createClient(cliente)
        handlerOpen()
      break;
      case 'actualizar':
        clients.updateClient(cliente)
        handlerOpen()
      break;
      default:
        break;
    }
    // clients.loadClient()
  }

  useEffect(() => {
  }, [])

  return (
    <>
        <MDBCol>
          <MDBCard>
            <MDBModalHeader>
              Listado de cliente
              <MDBBtn color="primary" onClick={()=>handlerOpen({
                  id: null,
                  nombre: '',
                  apellido: '',
                  numero_identificacion: '',
              })}>Crear</MDBBtn>
            </MDBModalHeader>
            <MDBCardBody>
              <ListaCliente listClients={clients.listClients} isLoading={clients.isLoading}  handlerAction={handlerAction} handlerOpen={handlerOpen} />
            </MDBCardBody>
          </MDBCard>
        <FormularioCliente clienteDefault={cliente} open={open} handlerOpen={handlerOpen} handlerAction={handlerAction}/>
        </MDBCol>
    </>
  );
}

export default App;
