import axios from 'axios';
import { BASE_URI } from '../commons/key';

const API = axios.create({
    baseURL: BASE_URI,
    withCredentials: false,
    headers: {
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      }
  });

  API.interceptors.response.use((response) => {
    if ( response.status != 200 && response.status != 201) {
        throw new Error (`Error código: ${response.status}`)
    }
    return response.data
}, (error) => {
    return Promise.reject(error.message);
});

export {
    API   
}