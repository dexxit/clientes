import { MDBListGroup, MDBListGroupItem, MDBContainer, MDBBtn, MDBCol, MDBRow } from "mdbreact";
import React, { memo, useEffect } from "react";
import { IClientResponse } from '../interface/IClient';

interface Props {
    isLoading: boolean,
    listClients: IClientResponse[],
    handlerAction: Function,
    handlerOpen: Function
}

export const ListaCliente = memo(( { listClients, isLoading, handlerAction, handlerOpen } : Props) => {

    useEffect(() => {
    }, [listClients])
    
    if (isLoading) {
        return (
            <h4>Espere por favor...</h4>
        )
    }
    return (
        <>
            <MDBContainer>
                <MDBListGroup >
                    {
                        listClients.map((cliente, index) => {
                            return (
                                <MDBListGroupItem key={index} className="d-flex justify-content-between align-items-center">
                                    {cliente.apellido +' ' +cliente.nombre}
                                    <MDBRow> 
                                        <MDBCol size="4"> <MDBBtn color="secondary" onClick={()=>handlerOpen(cliente)}>editar</MDBBtn></MDBCol>
                                        <MDBCol size="4"> <MDBBtn color="danger" onClick={()=>handlerAction(cliente, 'eliminar')}>eliminar</MDBBtn></MDBCol>
                                    </MDBRow>
                                </MDBListGroupItem>

                            )
                        } )
                    }
                </MDBListGroup>
            </MDBContainer>

        </>
    )
}
)