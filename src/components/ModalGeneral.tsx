import React, { useEffect, ReactElement, JSXElementConstructor } from 'react'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

interface Props {
    open: boolean,
    handlerOpen: Function,
    component: ReactElement<any, string | JSXElementConstructor<any>>
}

export const ModalGeneral = ( { open ,handlerOpen, component }: Props) => {

    useEffect(() => {
    }, [open])

    return (
        <MDBContainer>
            <MDBModal size="sm" isOpen={open} toggle={()=>(handlerOpen())} inline={false} noClickableBodyWithoutBackdrop overflowScroll >
                <MDBModalHeader toggle={()=>(handlerOpen())}>Mantenimiento</MDBModalHeader>
                <MDBModalBody>
                    {component}
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="secondary" onClick={()=>(handlerOpen())}>Close</MDBBtn>
                    <MDBBtn color="primary">Save changes</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        </MDBContainer>
    )

}
