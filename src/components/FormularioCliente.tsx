
import React, { useEffect, useState } from 'react'
import { MDBContainer, MDBRow, MDBModal, MDBModalHeader, MDBModalFooter, MDBModalBody, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';
import { IClientResponse } from '../interface/IClient';

interface Props {
    open: boolean,
    handlerOpen: Function,
    handlerAction: Function,
    clienteDefault: IClientResponse 
}

export const FormularioCliente = React.memo(( { open ,handlerOpen ,handlerAction, clienteDefault }: Props) => {

    const [cliente, setCliente] = useState<IClientResponse>({...clienteDefault})

   useEffect(() => {
        setCliente({...clienteDefault})
    }, [open, clienteDefault])
    
    return (
        <>
        <MDBContainer>
            <MDBModal isOpen={open} size="fluid" toggle={()=>(handlerOpen())} inline={false} noClickableBodyWithoutBackdrop overflowScroll >
                <MDBModalHeader toggle={()=>(handlerOpen())}>Mantenimiento</MDBModalHeader>
                <MDBModalBody>
                    <MDBRow>
                        <MDBCol md="12">
                            <MDBCard>
                                <MDBCardBody >
                                    <form>
                                        <div className="grey-text">
                                            <MDBInput
                                                label="Nombre"
                                                group
                                                value={cliente.nombre}
                                                onChange={(e)=> setCliente({...cliente,nombre:(e.target as HTMLInputElement).value})}
                                                type="text"
                                                validate
                                                error="wrong"
                                                success="right"
                                            />
                                            <MDBInput
                                                label="Apellido"
                                                group
                                                value={cliente.apellido}
                                                onChange={(e)=> setCliente({...cliente,apellido:(e.target as HTMLInputElement).value})}
                                                type="email"
                                                validate
                                                error="wrong"
                                                success="right"
                                            />
                                            <MDBInput
                                                label="Número de identificación"
                                                group
                                                value={cliente.numero_identificacion}
                                                onChange={(e)=> setCliente({...cliente,numero_identificacion:(e.target as HTMLInputElement).value})}
                                                type="number"
                                                validate
                                                error="wrong"
                                                success="right"
                                            />
                                        </div>
                                    </form>
                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="secondary" onClick={()=>(handlerOpen())}>Cerrar</MDBBtn>
                    <MDBBtn color="primary" onClick={()=>(handlerAction({...cliente}, (cliente.id == null ? 'crear' : 'actualizar')))}>Grabar</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        </MDBContainer>
        </>
    )
})
